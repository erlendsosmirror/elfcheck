# Readme #

This utility can be used to check that an elf file will load properly
on the target by "dummy" loading it. This program was used to develop
the code featured in the kernel, and is capable of printing more debug
information. You can use this program to test your programs after
building automatically by adding it to your makefile targets.

The code in the `elf.c` and `elf-reloc.c` files is the code that will
be used on a microcontroller, while the code in the `elf-debug.c` file
is mainly intended for desktop debugging. The code in the `main.c` file
illustrates how the elf-loader is used.

Options:

 * `-h` Print the elf header
 * `-s` Print section headers
 * `-d` Dump sections as hex
 * `-r` Dump relocation information
 * `-m` Print a summary of expected memory usage
