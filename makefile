all: build/elfcheck

clean:
	@rm -rf build

build/elfcheck:
	@mkdir -p build
	@gcc -o build/elfcheck -Iinc src/main.c src/elf.c src/elf-reloc.c src/elf-debug.c

test:
	@./build/elfcheck -m testprograms/build/testprogram.elf
