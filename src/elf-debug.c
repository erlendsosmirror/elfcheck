/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "elf.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void PrintLoader (struct Loader *l)
{
	printf ("Entry:       0x%.8X\n", l->entrypoint);
	printf ("SH Offset:   0x%.8X\n", l->shoff);
	printf ("SH NUm:      0x%.4X\n", l->shnum);
	printf ("shstrndx:    0x%.4X\n", l->shstrndx);
}

void PrintElfSection (struct ElfSection *s)
{
	printf ("nameoffset:  0x%.8X\n", s->nameoffset);
	printf ("offset:      0x%.8X\n", s->offset);
	printf ("size:        0x%.8X\n", s->size);
}

void PrintLoaderSection (struct LoaderSection *s, char *name)
{
	printf ("Name:        %s\n", name);
	printf ("offset:      0x%.8X\n", s->offset);
	printf ("size:        0x%.8X\n", s->size);
}

void PrintInfos (struct Loader *l)
{
	PrintLoaderSection (&l->text, "text");
	PrintLoaderSection (&l->rodata, "rodata");
	PrintLoaderSection (&l->data, "data");
	PrintLoaderSection (&l->bss, "bss");

	PrintLoaderSection (&l->rel_text, "rel.text");
	PrintLoaderSection (&l->rel_rodata, "rel.rodata");
	PrintLoaderSection (&l->rel_data, "rel.data");
	PrintLoaderSection (&l->rel_bss, "rel.bss");

	PrintLoaderSection (&l->symbols, "symtab");
}

void DumpChar (char *data, unsigned int size)
{
	for (int i = 0; i < size; i++)
	{
		char d = data[i];

		if (d < ' ' || d > '~')
			d = '.';

		printf ("%c", d);
	}

	printf ("\n");
}

void DumpHex (void *data, unsigned int size)
{
	unsigned int offset = 0;

	for (int i = 0; i < size / 16; i++, offset += 16, data += 16)
	{
		unsigned int *d = (unsigned int*) data;
		printf ("%.8X: %.8X %.8X %.8X %.8X ", offset, d[0], d[1], d[2], d[3]);
		DumpChar ((char*)data, 16);
	}

	printf ("%.8X: ", offset);

	for (int i = offset; i < size; i++)
		printf ("%.2X ", ((unsigned char*)data)[i]);

	DumpChar ((char*)data, size - offset);

	printf ("\n");
}

unsigned int PrintRelocType (int info, unsigned int *instruction)
{
	unsigned short *sh = (unsigned short*) instruction;

	switch (info & 0x00FF)
	{
	case 2:
		printf ("R_ARM_ABS32     : ");
		return *instruction;
	case 10:
		printf ("R_ARM_THM_CALL  : ");
		return (sh[0] << 16) | sh[1];
	case 30:
		printf ("R_ARM_THM_JUMP24: ");
		return (sh[0] << 16) | sh[1];
	default:
		printf ("Unknown relocation type\n\n");
		return 0;
	}
}

void PrintRelocEnd (int info, unsigned int offset, unsigned int symaddr, unsigned int oldinstruction, unsigned int *inst, unsigned int *boff)
{
	switch (info & 0x00FF)
	{
	case 2:
		printf ("offset %.8X symaddr %.8X instruction %.8X relocated %.8X\n",
			offset, symaddr, oldinstruction, inst[0]);
		break;
	case 10:
	case 30:
	{
		unsigned short *sh = (unsigned short*) inst;
		unsigned int newinstruction = (sh[0] << 16) | sh[1];
		printf ("offset %.8X symaddr %.8X instruction %.8X relocated %.8X off0 %.8X off1 %.8X\n",
			offset, symaddr, oldinstruction, newinstruction, boff[0], boff[1]);
		break;
	}
	default:
		break;
	}
}

int ErrorToString (int error)
{
	switch (error)
	{
	case EELFLDR_OK:
		printf ("No error\n");
		break;
	case EELFLDR_SEEK:
		printf ("Seek error\n");
		break;
	case EELFLDR_READ:
		printf ("Read error\n");
		break;
	case EELFLDR_MAGIC:
		printf ("Header error\n");
		break;
	case EELFLDR_VERSION:
		printf ("Version error\n");
		break;
	case EELFLDR_UNDEFEXTERNAL:
		printf ("There was undefined symbols\n");
		break;
	case EELFLDR_WOULDOVERFLOW:
		printf ("Invalid data would have caused an overflow\n");
		break;
	default:
		printf ("Unknown error\n");
		break;
	}

	return error;
}

void PrintMemoryUsage (struct Loader *l)
{
	printf ("Code memory:  %i\n", l->text.size);
	printf ("Data memory:  %i\n", l->rodata.size + l->data.size);
	printf ("Work memory:  %i\n", l->bss.size);
	printf ("Total memory: %i\n", l->text.size + l->rodata.size + l->data.size + l->bss.size);
}

void PrintUndefinedSymbol (struct Loader *l, int strindex, int symindex)
{
	char d[64];

	unsigned int maxlen = l->strings.size - strindex;

	if (maxlen > sizeof(d))
		maxlen = sizeof(d);

	int r = LoadSectionData (l, l->strings.offset + strindex, maxlen, d);

	if (r)
	{
		printf ("Error: Could not load name of symbol %i\n", symindex);
		return;
	}

	d[sizeof(d)-1] = 0;

	printf ("Undefined symbol %i: %s\n", symindex, d);
}
