/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "elf.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int CheckHeader (struct Loader *l)
{
	struct ElfHeader eh;

	int r = LoadSectionData (l, 0, sizeof (struct ElfHeader), &eh);

	if (r)
		return r;

	static const char magic[4] = {0x7f, 0x45, 0x4c, 0x46};

	if (memcmp (eh.magic, magic, 4))
		return EELFLDR_MAGIC;

	static const char versionpart[5+7+8] =
		{1, 1, 1, 0, 0,
		0, 0, 0, 0, 0, 0, 0,
		1, 0, 0x28, 0, 1, 0, 0, 0};

	static const char versionpart_cortex_r[5+7+8] =
		{1, 2, 1, 0, 0,
		0, 0, 0, 0, 0, 0, 0,
		0, 1, 0, 0x28, 0, 0, 0, 1};

	if (!memcmp (eh.versionpart, versionpart_cortex_r, sizeof (versionpart_cortex_r)))
		l->big = 1;
	else if (!memcmp (eh.versionpart, versionpart, sizeof (versionpart)))
		l->big = 0;
	else
		return EELFLDR_VERSION;

	l->entrypoint = CheckEndian (l, eh.entrypoint);
	l->shoff = CheckEndian (l, eh.shoff);
	l->shnum = CheckEndianShort (l, eh.shnum);
	l->shstrndx = CheckEndianShort (l, eh.shstrndx);

	return EELFLDR_OK;
}

int LoadSectionHeader (struct Loader *l, struct ElfSection *sh, int index)
{
	unsigned int seekto = l->shoff + ((unsigned int)index * 0x28);
	return LoadSectionData (l, seekto, sizeof (struct ElfSection), sh);
}

int LoadSectionData (struct Loader *l, unsigned int offset, unsigned int size, void *target)
{
	if (lseek (l->fd, offset, SEEK_SET) != offset)
		return EELFLDR_SEEK;

	if (read (l->fd, target, size) != size)
		return EELFLDR_READ;

	return EELFLDR_OK;
}

void FindSizesHelper (struct LoaderSection *dst, struct ElfSection *src, int i)
{
	dst->offset = src->offset;
	dst->size = src->size;
	dst->index = i;
}

int FindSizes (struct Loader *l)
{
	for (int i = 0; i < l->shnum; i++)
	{
		struct ElfSection sh;
		memset (&sh, 0, sizeof (sh));

		int r = LoadSectionHeader (l, &sh, i);

		if (r)
			return r;

		HeaderEndianCheck (l, &sh);

		if (sh.nameoffset >= SELFLDR_NAMES_SIZE)
			return EELFLDR_WOULDOVERFLOW;

		char *name = &l->names[sh.nameoffset];

		if (!strcmp (name, ".text"))
			FindSizesHelper (&l->text, &sh, i);
		else if (!strcmp (name, ".rodata"))
			FindSizesHelper (&l->rodata, &sh, i);
		else if (!strcmp (name, ".data"))
			FindSizesHelper (&l->data, &sh, i);
		else if (!strcmp (name, ".bss"))
			FindSizesHelper (&l->bss, &sh, i);
		else if (!strcmp (name, ".rel.text"))
			FindSizesHelper (&l->rel_text, &sh, i);
		else if (!strcmp (name, ".rel.rodata"))
			FindSizesHelper (&l->rel_rodata, &sh, i);
		else if (!strcmp (name, ".rel.data"))
			FindSizesHelper (&l->rel_data, &sh, i);
		else if (!strcmp (name, ".rel.bss"))
			FindSizesHelper (&l->rel_bss, &sh, i);
		else if (!strcmp (name, ".symtab"))
			FindSizesHelper (&l->symbols, &sh, i);
		else if (!strcmp (name, ".strtab"))
			FindSizesHelper (&l->strings, &sh, i);
	}

	return EELFLDR_OK;
}

int LoadData (struct Loader *l, struct Program *p)
{
	int r = 0;

	if (p->text && (r = LoadSectionData (l, l->text.offset, l->text.size, p->text)))
		return r;

	if (p->rodata && (r = LoadSectionData (l, l->rodata.offset, l->rodata.size, p->rodata)))
		return r;

	if (p->data && (r = LoadSectionData (l, l->data.offset, l->data.size, p->data)))
		return r;

	return EELFLDR_OK;
}
