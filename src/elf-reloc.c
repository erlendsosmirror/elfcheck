/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "elf.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int ResolveSymbolAddress (int *symaddr, struct Loader *l, struct Program *p)
{
	// Symbol entries are 16 bytes
	unsigned int entry[4];

	// Load the entry
	int r = LoadSectionData (l, l->symbols.offset + (16*(*symaddr)), 16, entry);

	if (r)
		return r;

	// Load the section index of the symbol (last short)
	int section = CheckEndianShort (l, ((short*)entry)[7]);

	// Resolve the section index into an address
	if (l->text.index == section)
		*symaddr = (unsigned long long) p->text;
	else if (l->rodata.index == section)
		*symaddr = (unsigned long long) p->rodata;
	else if (l->data.index == section)
		*symaddr = (unsigned long long) p->data;
	else if (l->bss.index == section)
		*symaddr = (unsigned long long) p->bss;
	else
	{
		PrintUndefinedSymbol (l, CheckEndian (l, entry[0]), *symaddr);
		return EELFLDR_UNDEFEXTERNAL;
	}

	// Add the offset of the symbol to the section base address and return
	*symaddr += CheckEndian (l, entry[1]);
	return EELFLDR_OK;
}

/*
 * Relocation of the types R_ARM_ABS32 and R_ARM_THM_CALL are fairly straight
 * forward. They are explained in the "ELF for the ARM Architecture"
 * document.
 *
 * R_ARM_THM_JUMP24 is a bit more difficult. The ELF document only explains
 * that the first 11 bits of the first half-word encode some bits and that
 * the first 11 bits of the second half word encode some more bits. It also
 * says that for ARMv6 and later there are additional bits.
 *
 * For the proper encoding, the "ARM Architecture Reference Manual" has to be
 * checked. In summary, the b instruction is encoded as follows:
 *
 * 54321098765432105432109876543210 (Bit indexes for half-words)
 * 10987654321098765432109876543210 (Bit indexes for words)
 * 11110Siiiiiiiiii10J1Jyyyyyyyyyyy (Instruction encoding)
 * 0000000SJJiiiiiiiiiiyyyyyyyyyyy0 (As decoded immediate)
 *
 * These rules are also given:
 *   I1 = NOT(J1 EOR S)
 *   I2 = NOT(J2 EOR S)
 *   imm32 = SignExtend(S:I1:I2:imm10:imm11:'0')
 */
int RelocateSymbol (unsigned int offset, int info, void *data, struct Loader *l, struct Program *p)
{
	// Resolve the symbol address
	unsigned int symaddr = info >> 8;

	int r = ResolveSymbolAddress (&symaddr, l, p);

	if (r)
		return r;

	// Instruction pointers
	unsigned int *instruction = data + offset;
	unsigned short *sh = (unsigned short*) instruction;
	unsigned int boff[2];

	// Debug print
	unsigned int oldinstruction = 0;

	if (l->args & OELFLDR_RDUMP)
		oldinstruction = PrintRelocType (info, instruction);

	// Relocate
	switch (info & 0x00FF)
	{
	case 2: // R_ARM_ABS32: (S + A) | T
		instruction[0] += symaddr;
		break;
	case 10: // R_ARM_THM_CALL: ((S + A) | T) - P
		// Fall through
	case 30: // R_ARM_THM_JUMP24: ((S + A | T) - P
	{
		// Decode instruction
		unsigned int branchoffset = ((sh[0] & 0x0400) << 14) | ((sh[0] & 0x3FF) << 12) | ((sh[1] & 0x7FF) << 1);

		unsigned int j = sh[1] & 0x2800;
		j = ((j >> 1) | j) & 0x1800;
		j ^= 0x1800;
		branchoffset |= j << 11;

		if (branchoffset & 0x01000000)
			branchoffset ^= 0xFEC00000;

		// ((S + A) | T) - P
		boff[0] = branchoffset;
		branchoffset = (symaddr + branchoffset) - (unsigned long long) sh;
		boff[1] = branchoffset;

		// Encode instruction
		if (branchoffset & 0x01000000)
			branchoffset ^= 0xFEC00000;

		j = branchoffset >> 11;
		j ^= 0x1800;
		j = ((j << 1) | j) & 0x2800;

		sh[0] = (sh[0] & 0xF800) | ((branchoffset >> 12) & 0x03FF);
		sh[1] = (sh[1] & 0xD000) | ((branchoffset >> 1) & 0x07FF) | j;
		break;
	}
	case 47: // R_ARM_THM_MOVW_ABS_NC (S + A) | T
		{
			instruction[0] |= symaddr & 0x00FF;
			instruction[0] |= (symaddr & 0x0700) << 4;
			instruction[0] |= (symaddr & 0x0800) << 15;
			instruction[0] |= (symaddr & 0xF000) << 4;
		}
		break;
	case 48: // R_ARM_THM_MOVT_ABS (S + A)
		{
			symaddr = symaddr >> 16;
			instruction[0] |= symaddr & 0x00FF;
			instruction[0] |= (symaddr & 0x0700) << 4;
			instruction[0] |= (symaddr & 0x0800) << 15;
			instruction[0] |= (symaddr & 0xF000) << 4;
		}
		break;
	default:
		return EELFLDR_UNDEFRELOC;
	}

	// Debug print
	if (l->args & OELFLDR_RDUMP)
		PrintRelocEnd (info, offset, symaddr, oldinstruction, instruction, boff);

	// Done
	return EELFLDR_OK;
}

int RelocateBlock (struct Loader *l, unsigned int offset, int n, unsigned int *data, struct Program *p)
{
	// Holds 64 bytes, 8 relocs at a time
	unsigned int blockdata[16];

	// Load the data
	int r = LoadSectionData (l, offset, n*8, blockdata);

	if (r)
		return r;

	for (int i = 0; i < 16; i++)
		blockdata[i] = CheckEndian (l, blockdata[i]);

	// Relocate the symbols
	int ret = EELFLDR_OK;

	for (int j = 0; j < n; j++)
		if ((r = RelocateSymbol (blockdata[j*2], blockdata[j*2+1], data, l, p)))
			ret = r;

	return ret;
}

int RelocateSection (struct Loader *l, struct LoaderSection *lf, unsigned int *data, struct Program *p)
{
	// Dump the section data
	if (l->args & OELFLDR_RDUMP)
	{
		unsigned int *rel = malloc (lf->size);
		LoadSectionData (l, lf->offset, lf->size, rel);
		printf ("Reloc:\n");
		DumpHex (rel, lf->size);
		free (rel);
	}

	// 8 relocs at a time
	unsigned int nrelocs = lf->size / 8;
	unsigned int nrem = nrelocs % 8;
	int i = 0;
	int r = 0;

	// Debug print
	if (l->args & OELFLDR_RDUMP)
		printf ("Number of relocations: %i\n", nrelocs);

	// Process blocks of 8 relocs
	for (; i < (nrelocs / 8); i++)
		if ((r = RelocateBlock (l, lf->offset + (i * 64), 8, data, p)))
			return r;

	// Process the remainder
	if ((r = RelocateBlock (l, lf->offset + (i * nrem * 8), nrem, data, p)))
		return r;

	return EELFLDR_OK;
}

int Relocate (int fd, struct Loader *l, struct Program *p)
{
	int r = EELFLDR_OK;

	if (l->rel_text.size && (r = RelocateSection (l, &l->rel_text, p->text, p)))
		return r;

	if (l->rel_rodata.size && (r = RelocateSection (l, &l->rel_rodata, p->rodata, p)))
		return r;

	if (l->rel_data.size && (r = RelocateSection (l, &l->rel_data, p->data, p)))
		return r;

	if (l->rel_bss.size && (r = RelocateSection (l, &l->rel_bss, p->bss, p)))
		return r;

	return EELFLDR_OK;
}
