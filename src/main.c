/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "elf.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int ParseArgument (const char *str)
{
	int args = 0;

	while (*str)
	{
		if (*str == 'h')
			args |= OELFLDR_PHEADER;
		else if (*str == 's')
			args |= OELFLDR_SHEADER;
		else if (*str == 'd')
			args |= OELFLDR_DUMP;
		else if (*str == 'r')
			args |= OELFLDR_RDUMP;
		else if (*str == 'm')
			args |= OELFLDR_PMEMUSE;

		str++;
	}

	return args;
}

void ParseArgs (int argc, char **argv, struct Loader *l)
{
	for (int i = 1; i < argc; i++)
	{
		if (!strcmp (argv[i], "--help"))
		{
			printf ("Usage: elfcheck <options> file.elf\n"
				"  -h Print the elf header\n"
				"  -s Print section headers\n"
				"  -d Dump sections as hex\n"
				"  -r Dump relocation information\n"
				"  -m Print a summary of expected memory usage\n");
			exit (0);
		}
		else if (argv[i][0] == '-')
			l->args |= ParseArgument (&argv[i][1]);
		else
			l->filename = argv[i];
	}
}

unsigned int CheckEndian (struct Loader *l, unsigned int a)
{
	if (l->big)
	{
		unsigned int b = 0;
		b |= (a >> 24) & 0x000000FF;
		b |= (a >> 8) & 0x0000FF00;
		b |= (a << 8) & 0x00FF0000;
		b |= (a << 24) & 0xFF000000;
		return b;
	}
	else
		return a;
}

unsigned short CheckEndianShort (struct Loader *l, unsigned short a)
{
	if (l->big)
	{
		unsigned short b = 0;
		b |= (a >> 8) & 0x00FF;
		b |= (a << 8) & 0xFF00;
		return b;
	}
	else
		return a;
}

void HeaderEndianCheck (struct Loader *l, struct ElfSection *sh)
{
	sh->nameoffset = CheckEndian (l, sh->nameoffset);
	sh->offset = CheckEndian (l, sh->offset);
	sh->size = CheckEndian (l, sh->size);
}

int main (int argc, char **argv)
{
	// Declare loader struct
	struct Loader l;
	memset (&l, 0, sizeof (struct Loader));

	// Check arguments
	ParseArgs (argc, argv, &l);

	// Open input file
	int fd = open (l.filename, O_RDONLY);
	l.fd = fd;

	if (fd < 0)
	{
		printf ("Could not open file\n");
		return 1;
	}

	// Return value
	int r;

	// Check the header
	if ((r = CheckHeader (&l)))
		return ErrorToString (r);

	// Print header
	if (l.args & OELFLDR_PHEADER)
		PrintLoader (&l);

	// Load names section header
	struct ElfSection shn;

	if ((r = LoadSectionHeader (&l, &shn, l.shstrndx)))
		return ErrorToString (r);

	HeaderEndianCheck (&l, &shn);

	l.shnames_offset = shn.offset;

	if (shn.size >= sizeof (l.names))
	{
		printf ("Warning: Naming truncated\n");
		shn.size = sizeof (l.names) - 1;
	}

	// Load names
	if ((r = LoadSectionData (&l, shn.offset, shn.size, l.names)))
		return ErrorToString (r);

	l.names[sizeof (l.names) - 1] = 0;

	// Calculate the required sizes
	if ((r = FindSizes (&l)))
		return ErrorToString (r);

	// Print section headers
	if (l.args & OELFLDR_SHEADER)
		PrintInfos (&l);

	// Dummy allocation
	struct Program p;
	p.text = malloc (l.text.size);
	p.rodata = malloc (l.rodata.size);
	p.data = malloc (l.data.size);
	p.bss = malloc (l.bss.size);

	// Load data
	if ((r = LoadData (&l, &p)))
		return ErrorToString (r);

	// Print hex dumps
	if (l.args & OELFLDR_DUMP)
	{
		printf ("\n\nText:\n");
		DumpHex (p.text, l.text.size);

		printf ("\n\nRoData:\n");
		DumpHex (p.rodata, l.rodata.size);

		printf ("\n\nData:\n");
		DumpHex (p.data, l.data.size);
	}

	// Relocate
	if ((r = Relocate (fd, &l, &p)))
		return ErrorToString (r);

	// Print statistics
	if (l.args & OELFLDR_PMEMUSE)
		PrintMemoryUsage (&l);

	// Done
	return 0;
}
